# FastApi Server
import base64
import binascii
import hmac
import hashlib
import json
from typing import Dict, Optional
from fastapi import FastAPI, Form, Cookie, Body
from fastapi.responses import Response



app = FastAPI()

SECRET_KEY = "bd8a282e689bf1da8db5525c3d557dc70580b4e94e8b5a42e35c44a4ed0183b2"
PASSWORD_SALT = "a21ebf9230e6ff9496703e394991c4f49aa29ef0811cd6f29548b69c5b9f1110"


def sign_data(data: str) -> str:
    """Возвращает подписанные данные data"""
    return hmac.new(
        SECRET_KEY.encode(),
        msg=data.encode(),
        digestmod=hashlib.sha256
    ).hexdigest().upper()


def get_username_from_signed_string(username_signed: str) -> Optional[str]:
    username_base64, sign = username_signed.split(".")
    #print("username_base64", username_base64)
    try:
        username = base64.b64decode(username_base64.encode()).decode()
        valid_sign = sign_data(username)
    except binascii.Error:
        print("Сработало исключение, возврат None")
        return None
    if hmac.compare_digest(valid_sign, sign):
        return username


def verify_password(username: str, password: str) -> bool:
    password_hash = hashlib.sha256((password + PASSWORD_SALT).encode())\
        .hexdigest().lower()
    stored_password_hash = users[username]['password'].lower()
    return  password_hash == stored_password_hash

users = {
    "alexey@user.com": {
        "name": "Алексей",
        "password": "d7a23655e4e8f5626426c0dabd3a2e3cd186098e46b3abe69bd09299b3e7486a",
        "balance": 100_000
    },
    "anatoliy@user.com": {
        "name": "Анатолий",
        "password": "083ed1e7226eb763d1016b319b1f63f059ad3de8b338fdefc4b4e7aef4273314",
        "balance": 500_000
    }
}



@app.get("/")
# В fastapi когда придет get запрос на адрес / запустится функция
def index_page(username: Optional[str] = Cookie(default=None)):
    with open('templates/login.html', 'r') as f:
        login_page = f.read()
    if not username:
        return Response(login_page, media_type="text/html")

    valid_username = get_username_from_signed_string(username)
    if not valid_username:
        response =  Response(login_page, media_type="text/html")
        response.delete_cookie(key="username")
        return response

    try:
        user = users[valid_username]
    except KeyError:
        response =  Response(login_page, media_type="text/html")
        response.delete_cookie(key="username")
        return response

    return Response(
        f"Привет, {users[valid_username]['name']}!<br />"
        f"Баланс: {users[valid_username]['balance']} евро",
        media_type="text/html")
    

@app.post("/login")
def process_login_page(data: dict = Body(...)):
    #print("data is: ", data)
    username = data["username"]
    password = data["password"]
    user = users.get(username)
    if not user or not verify_password(username, password):
        return Response(
            json.dumps({
                "success": False,
                "message": f"уважаемый {username}, я вас не знаю"
            }),
            media_type="application/json")

    response = Response(
        json.dumps({
            "success": True,
            "message": f"Привет, {user['name']}<br />Баланс: {user['balance']} евро"
        }),
        media_type="application/json")

    username_signed = base64.b64encode(username.encode()).decode() + \
        "." + sign_data(username)
    response.set_cookie(key="username", value=username_signed)
    return response

